# !/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, os, random, time
import matplotlib
matplotlib.use('Qt5Agg')
from PyQt5 import QtCore, QtWidgets, QtGui
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from numpy import arange, sin, pi
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
from embedded import Arduino

progname = "Temperature"
progversion = "1.0"
y = [0, 1, 2, 3]
x = [0, 1, 2, 3]
cont = x[3]

class MyMplCanvas(FigureCanvas):
  def __init__(self, parent=None, width=5, height=4, dpi=100):
    fig = Figure(figsize=(width, height), dpi=dpi)
    self.axes = fig.add_subplot(111)
    self.compute_initial_figure()
    FigureCanvas.__init__(self, fig)
    self.setParent(parent)
    FigureCanvas.setSizePolicy(self, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
    FigureCanvas.updateGeometry(self)
    pass

  def compute_initial_figure(self):
    pass
  pass


class MyStaticMplCanvas(MyMplCanvas):
  def __init__(self, *args, **kwargs):
    MyMplCanvas.__init__(self, *args, **kwargs)
    timer = QTimer(self)
    timer.setInterval(100)
    timer.timeout.connect(self.update_figure)
    timer.start()
    pass

  def compute_initial_figure(self):
    self.axes.plot(x, y, 'orangered', marker='.')
    pass

  def update_figure(self):
    global x, cont
    y.remove(y[0])
    y.append(Arduino.communication())
    x.remove(x[0])
    cont += 1
    x.append(cont)
    self.axes.cla()
    self.axes.plot(x, y, 'orangered', marker='.')
    self.axes.patch.set_facecolor('#3d3d3d')
    self.axes.grid(True)
    self.axes.set_xlabel('Registry')
    self.axes.set_ylabel('Temperature')
    self.draw()
    pass
  pass


class ApplicationWindow(QtWidgets.QMainWindow):
  def __init__(self):
    QtWidgets.QMainWindow.__init__(self)
    self.resize(800, 600)
    self.setWindowTitle(progname + " - v." + progversion)
    self.setWindowIcon(QtGui.QIcon('icon.ico'))
    self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
    self.file_menu = QtWidgets.QMenu('&Arduino', self)
    self.file_menu.addAction('&Status', self.arduinoInfo, QtCore.Qt.CTRL + QtCore.Qt.Key_S)
    self.file_menu.addAction('&Port', self.portInfo, QtCore.Qt.CTRL + QtCore.Qt.Key_P)
    self.file_menu.addSeparator()
    self.file_menu.addAction('&Quit', self.fileQuit, QtCore.Qt.CTRL + QtCore.Qt.Key_Q)
    self.menuBar().addMenu(self.file_menu)
    self.help_menu = QtWidgets.QMenu('&Help', self)
    self.menuBar().addSeparator()
    self.menuBar().addMenu(self.help_menu)
    self.help_menu.addAction('&About', self.about)
    self.main_widget = QtWidgets.QWidget(self)
    l = QtWidgets.QVBoxLayout(self.main_widget)
    sc = MyStaticMplCanvas(self.main_widget, width=5, height=4, dpi=100)
    l.addWidget(sc)
    self.main_widget.setFocus()
    self.setCentralWidget(self.main_widget)
    self.statusBar().showMessage("Loading...", 2000)
    pass

  def arduinoInfo(self):
    if connectionState:
      QtWidgets.QMessageBox.about(self, "Arduino", "Arduino connection is stable.")
    else:
      QtWidgets.QMessageBox.about(self, "Arduino", "Arduino connection is lost.")
    pass

  def portInfo(self):
    QtWidgets.QMessageBox.about(self, "Port", "Getting info at port: " + connectionState)
    pass

  def fileQuit(self):
    self.close()
    pass

  def closeEvent(self, ce):
    self.fileQuit()
    pass

  def about(self):
    QtWidgets.QMessageBox.about(self, "About", 
    """
    Author: Azael Rodríguez
    License: MIT
    """)
    pass
  pass

if __name__ == "__main__":
  connectionState = Arduino.verifyConnection()
  qApp = QtWidgets.QApplication(sys.argv)
  aw = ApplicationWindow()
  aw.show()
  sys.exit(qApp.exec_())
