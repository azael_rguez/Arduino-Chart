#include <DHT.h>

int sensor = 13;

DHT dht (sensor, DHT11);

void setup(){
  Serial.begin(9600);
  dht.begin();
}

void loop(){
  Serial.println(dht.readTemperature());
  delay(100);
}

