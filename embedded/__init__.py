# !/usr/bin/env python
# -*- coding: utf-8 -*-
import serial, time
import serial.tools.list_ports

class Arduino:
    def verifyConnection():
        try:
            global serialPort
            serialPort = ""
            ports = list(serial.tools.list_ports.comports())
            for p in ports:
                aux = p
                serialPort = str(aux)[:4]
            ser = serial.Serial(serialPort, 9600, timeout=0)
            return serialPort
        except serial.SerialException as e:
            return ""
        pass

    def communication():
        ser = serial.Serial(serialPort, 9600)
        return float(ser.readline().decode("utf-8"))
        pass

    pass